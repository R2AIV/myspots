/*
 *
 * CW spotting tool via reversebeacon.net
 * Created by R2AIV 230921
 * Modified 		031122
 * Modified 		081122
 * Fixed bugs		092222
 * All issues please send to l0calhost[at]mail.ru
 * 
 * This software under development. You can use it how you want.
 * Fuck all copyrights and licences.
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#include <winsock2.h>
#include <windows.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <string.h>

#define USE_WINSOCK

#define SRV_ADDR 	"216.93.248.68"
// #define SRV_ADDR   "198.137.202.75"

#define SRV_PORT	7000     // For CW
// #define SRV_PORT	7001	 // For FT8

// For compiling undex UNIX systems
#ifndef USE_WINSOCK
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#endif

void *MessageThreadFunc(void *Message)
{
	char MsgText[100];
	sprintf(MsgText, "%s ON AIR NOW!!!", (char *)Message);
	MessageBox(NULL, MsgText, "MySpots", MB_ICONINFORMATION);		
};

int ServerConnect()
{
	int cli_sock = 0;
	int stat = 0;
	struct sockaddr_in cli_addr;
	struct hostent *srv;
	
#ifdef USE_WINSOCK
	WORD wVersionRequested;
	WSADATA wsaData;
	
	wVersionRequested = MAKEWORD(2, 2);
	WSAStartup(wVersionRequested, &wsaData);	
#endif	

	cli_sock = socket(AF_INET, SOCK_STREAM, 0);

	if(cli_sock < 0)
	{
		printf("Socket creation error!\r\n");
		exit(-1);
	};

	srv = gethostbyname("telnet.reversebeacon.net");

	if(srv == NULL)
	{
		printf("Server name translation error!\r\n");
		exit(-1);
	};

	cli_addr.sin_family = AF_INET;
	cli_addr.sin_port = htons(SRV_PORT);	
	cli_addr.sin_addr.s_addr = inet_addr(SRV_ADDR);

	// cli_addr.sin_addr.s_addr= inet_addr("telnet.reversebeacon.net");

	stat = connect(cli_sock, (struct sockaddr *)&cli_addr, sizeof(cli_addr));
	if(stat == -1)
	{
		printf("Connection error!\r\n");
		exit(-1);
	}
	else if(stat == 0)
	{
		printf("Connection OK! Using IP: %s, port: %d\r\n", SRV_ADDR, SRV_PORT);
	}
	
#ifdef USE_WINSOCK
//	WSACleanup();
#endif

	return cli_sock;
}

int main(int argc, char *argv[])
{
	int cli_sock;
	char StringBuffer[2048];
	char StringToPrint[100];
	char UserMessage[100];
	char MyCall[20];
	char SpotCall[20];
	FILE *RawLogFile;
	
	system("cls");

	printf("-=<CW spotting tool via reversebeacon.net by R2AIV Ver 0.0.5>=-\r\n");
	printf("Compiled at: %s %s\r\n\r\n", __DATE__, __TIME__);
	
	if(argc == 3)
	{
		printf("Looking for call: %s\r\n", argv[2]);
	}
	
	pthread_t MsgThread;
	
	if(argc < 2)
	{
		printf("USAGE: myspots [your CALLSIGN] [search string]\r\n");
		exit(0);
	};

	cli_sock = ServerConnect();

	// Fake receive for "Enter your call" message
	while(strstr(StringBuffer, "SK1MMR") == NULL) 
	{
		memset((void *)StringBuffer,0,sizeof(StringBuffer));
		recv(cli_sock, StringBuffer, sizeof(StringBuffer), 0);
		strcpy(MyCall, argv[1]);
		memcpy((void *)StringBuffer, MyCall, strlen(MyCall));
		
		send(cli_sock, StringBuffer, strlen(MyCall), 0);
		send(cli_sock, "\r\n", 2, 0);
	};

	while(1)
	{
		RawLogFile = fopen("rawlog.txt","a+");
		memset((void *)StringBuffer,0,sizeof(StringBuffer));
		recv(cli_sock, StringBuffer, sizeof(StringBuffer), 0);
		fprintf(RawLogFile, "%s" ,StringBuffer);				
		fclose(RawLogFile);
		strcpy(StringToPrint, StringBuffer);

		if(argc == 2)
		{
			printf("%s", StringToPrint);
			continue;			
		}

		// Filter searched call
		if(strstr(StringToPrint, argv[2]) != NULL) 
		{			
			pthread_create(&MsgThread, NULL, MessageThreadFunc, (void *)argv[2]);
			// pthread_join(MsgThread, NULL);
			printf("%s", StringToPrint);
		}

		memset((void *)StringBuffer,0,sizeof(StringBuffer));
		memset((void *)StringToPrint,0,sizeof(StringToPrint));
		memset((void *)UserMessage,0,sizeof(UserMessage));
	};
};

